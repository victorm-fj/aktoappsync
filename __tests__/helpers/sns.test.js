import AWS from 'aws-sdk-mock';
import { call } from '../../src/helpers/sns';

describe('SNS', () => {
	afterEach(() => {
		AWS.restore('SNS');
	});

	it('should call the dynamoDB client instance with specified parameters', async () => {
		const invitationCode = 'EDFG67J';
		const downloadURL = 'akto.io';
		const phoneNumber = '+541238964573';
		const action = 'publish';
		const params = {
			Message: `You have been invited to join AKTO, international intelligence agency. Here is your code: ${invitationCode}. Download the app here ${downloadURL}`,
			MessageAttributes: {
				'AWS.SNS.SMS.SenderID': {
					DataType: 'String',
					StringValue: 'aktoio',
				},
				'AWS.SNS.SMS.SMSType': {
					DataType: 'String',
					StringValue: 'Transactional',
				},
			},
			PhoneNumber: phoneNumber,
		};
		const response = { MessageId: '9b888f80-15f7-5c30-81a2-c4511a3f5229' };
		AWS.mock('SNS', action, (params, callback) => {
			expect(params).toEqual(params);
			callback(null, response);
		});
		const data = await call(action, params);
		expect(data).toEqual(response);
	});
});
