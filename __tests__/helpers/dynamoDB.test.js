import AWS from 'aws-sdk-mock';
import { call } from '../../src/helpers/dynamodb';

describe('DynamoDB', () => {
	afterEach(() => {
		AWS.restore('DynamoDB.DocumentClient');
	});

	it('should call the dynamoDB client instance with specified parameters', async () => {
		const tableName = 'ActiveUsers';
		const item = { userId: 'AEDF56Y' };
		const action = 'get';
		const params = { TableName: tableName, Key: item };
		const response = { Item: { userId: 'AEDF56Y' } };
		AWS.mock('DynamoDB.DocumentClient', action, (params, callback) => {
			expect(params.TableName).toEqual(tableName);
			expect(params.Key).toEqual(item);
			callback(null, response);
		});
		const data = await call(action, params);
		expect(data).toEqual(response);
	});
});
