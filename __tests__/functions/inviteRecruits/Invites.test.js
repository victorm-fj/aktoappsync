import Invites from '../../../src/functions/inviteRecruits/Invites';

describe('Invites', () => {
	const userId = 'us-east-1:3fbbd145-5f54-4e45-8e36-9bb70ea1defx';
	const username = 'FGH56HJ';
	const invitationId = 'RG67UJB';
	const recruits = [
		{
			recordId: '1244DGGHBBBX567B:Abc',
			phoneNumber: '+527562931284',
		},
		{
			recordId: '1244DGGHB43567B:Abc',
			phoneNumber: '+524569431284',
		},
	];
	const downloadURL = 'akto.io';
	const userResponse = { Item: { userId, username, invitationId } };
	const sendMessageResponse = [
		{
			ResponseMetadata: { RequestId: '7e373951-69ae-5896-a597-6a6913f501a6' },
			MessageId: 'f4035340-d18a-531c-9609-0078abd16a2c',
		},
		{
			ResponseMetadata: { RequestId: 'bffe24e2-ee2b-5c79-b767-77da9470c4ab' },
			MessageId: '36f26f73-de46-5cb2-8faa-3c4ba41d2460',
		},
	];

	test('successfully send invite', async () => {
		const isUser = jest.fn(() => Promise.resolve(userResponse));
		const sendMessage = jest.fn(() => Promise.resolve(sendMessageResponse));
		const callback = jest.fn();
		const invites = new Invites(isUser, sendMessage);
		await invites.send(userId, [recruits[0]], downloadURL, callback);
		expect(isUser).toHaveBeenCalledWith(userId);
		expect(sendMessage).toHaveBeenCalledTimes(1);
		expect(sendMessage).toHaveBeenCalledWith(
			recruits[0].phoneNumber,
			invitationId,
			downloadURL
		);
		expect(callback).toHaveBeenCalledWith(null, [recruits[0]]);
	});

	test('successfully send invites', async () => {
		const isUser = jest.fn(() => Promise.resolve(userResponse));
		const sendMessage = jest.fn(() => Promise.resolve(sendMessageResponse));
		const callback = jest.fn();
		const invites = new Invites(isUser, sendMessage);
		await invites.send(userId, recruits, downloadURL, callback);
		expect(isUser).toHaveBeenCalledWith(userId);
		expect(sendMessage).toHaveBeenCalledTimes(2);
		expect(callback).toHaveBeenCalledWith(null, recruits);
	});

	test('return null when error', async () => {
		const error = new Error('Something went wrong');
		const isUser = jest.fn(() => Promise.resolve(userResponse));
		const sendMessage = jest.fn(() => Promise.reject(error));
		const callback = jest.fn();
		const invites = new Invites(isUser, sendMessage);
		await invites.send(userId, recruits, downloadURL, callback);
		expect(isUser).toHaveBeenCalledWith(userId);
		expect(sendMessage).toHaveBeenCalledTimes(2);
		expect(callback).toHaveBeenCalledWith(error, null);
	});
});
