import NewUsers from '../../../src/functions/newUser/NewUsers';

describe('NewUsers', () => {
	const userId = 'us-east-1:3fbbd145-5f54-4e45-8e36-9bb70ea1defx';
	const username = 'FGH56HJ';
	const invitationId = 'RG67UJB';
	const recruiterId = 'ADF5GHY';
	const recruiter = {
		userId,
		username,
		invitationId,
		recruiter: recruiterId,
		recruits: [],
	};
	const isInvitationResponse = {
		Items: [recruiter],
		Count: 1,
		ScannedCount: 1,
	};
	const newUserId = 'us-east-1:3fbbd145-5f54-4e45-8e36-9bb56ji1dhua';
	const newUsername = 'AD56HJK';
	const newInvitationId = 'EGG56FZ';
	const newUser = {
		userId: newUserId,
		username: newUsername,
		invitationId: newInvitationId,
		recruiter: username,
		recruits: [],
	};

	test('saves new user to database', async () => {
		const isInvitation = jest.fn(() => Promise.resolve(isInvitationResponse));
		const uniqueId = jest.fn(() => Promise.resolve(newInvitationId));
		const saveUser = jest.fn(() => Promise.resolve());
		const callback = jest.fn();
		const newUsers = new NewUsers(uniqueId, isInvitation, saveUser);
		await newUsers.save(newUserId, newUsername, invitationId, callback);
		expect(isInvitation).toHaveBeenCalledTimes(1);
		expect(uniqueId).toHaveBeenCalledWith(isInvitation);
		expect(saveUser).toHaveBeenCalledTimes(2);
		expect(saveUser.mock.calls[0][0]).toEqual({
			...recruiter,
			recruits: [newUsername],
		});
		expect(saveUser.mock.calls[1][0]).toEqual(newUser);
		expect(callback).toHaveBeenCalledWith(null, newUser);
	});

	test('return null when error', async () => {
		const error = new Error('Something went wrong');
		const isInvitation = jest.fn(() => Promise.reject(error));
		const uniqueId = jest.fn(() => Promise.resolve());
		const saveUser = jest.fn(() => Promise.reject());
		const callback = jest.fn();
		const newUsers = new NewUsers(uniqueId, isInvitation, saveUser);
		await newUsers.save(newUserId, newUsername, invitationId, callback);
		expect(isInvitation).toHaveBeenCalledWith(invitationId);
		expect(saveUser).not.toHaveBeenCalled();
		expect(uniqueId).not.toHaveBeenCalled();
		expect(callback).toHaveBeenCalledWith(error, null);
	});
});
