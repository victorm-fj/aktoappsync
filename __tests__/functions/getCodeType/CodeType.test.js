import CodeType from '../../../src/functions/getCodeType/CodeType';

describe('CodeType', () => {
	const userId = 'us-east-1:3fbbd145-5f54-4e45-8e36-9bb70ea1defx';
	const username = 'UUKXQEZ';
	const invitationId = 'FS6HJ7L';
	const user = { userId, username, invitationId };
	const noValidQueryResponse = {
		Items: [],
		Count: 0,
		ScannedCount: 0,
	};
	const validQueryResponse = {
		Items: [user],
		Count: 1,
		ScannedCount: 1,
	};
	test('codeId belongs to an active user username', async () => {
		const codeId = 'UUKXQEZ';
		const isInvitation = jest.fn(() => Promise.resolve(noValidQueryResponse));
		const isUsername = jest.fn(() => Promise.resolve(validQueryResponse));
		const uniqueId = jest.fn();
		const callback = jest.fn();
		const codeTypeResponse = {
			codeId,
			username: '',
			isInvitation: false,
			isUsername: true,
		};
		const codeType = new CodeType(isInvitation, isUsername, uniqueId);
		await codeType.get(codeId, callback);
		expect(isInvitation).toHaveBeenCalledWith(codeId);
		expect(uniqueId).not.toBeCalled();
		expect(isUsername).toHaveBeenCalledWith(codeId);
		expect(callback).toBeCalledWith(null, codeTypeResponse);
	});

	test('codeId is a valid invitationId', async () => {
		const codeId = 'FS6HJ7L';
		const isInvitation = jest.fn(() => Promise.resolve(validQueryResponse));
		const isUsername = jest.fn();
		const uniqueId = jest.fn(() => Promise.resolve(newUsernameId));
		const callback = jest.fn();
		const newUsernameId = 'GTYH7KI';
		const codeTypeResponse = {
			codeId,
			username: newUsernameId,
			isInvitation: true,
			isUsername: false,
		};
		const codeType = new CodeType(isInvitation, isUsername, uniqueId);
		await codeType.get(codeId, callback);
		expect(isInvitation).toHaveBeenCalledWith(codeId);
		expect(uniqueId).toBeCalled();
		expect(isUsername).not.toBeCalled();
		expect(callback).toBeCalledWith(null, codeTypeResponse);
	});

	test('codeId in not a valid codeType', async () => {
		const codeId = 'FHA56GJ';
		const isInvitation = jest.fn(() => Promise.resolve(noValidQueryResponse));
		const isUsername = jest.fn(() => Promise.resolve(noValidQueryResponse));
		const uniqueId = jest.fn();
		const callback = jest.fn();
		const codeTypeResponse = {
			codeId,
			username: '',
			isInvitation: false,
			isUsername: false,
		};
		const codeType = new CodeType(isInvitation, isUsername, uniqueId);
		await codeType.get(codeId, callback);
		expect(isInvitation).toHaveBeenCalledWith(codeId);
		expect(uniqueId).not.toBeCalled();
		expect(isUsername).toHaveBeenCalledWith(codeId);
		expect(callback).toBeCalledWith(null, codeTypeResponse);
	});

	test('codeId is neither an invitationId nor a user username when error', async () => {
		const codeId = 'FS6HJ7L';
		const error = new Error('Something went wrong');
		const isInvitation = jest.fn(() => Promise.reject(error));
		const isUsername = jest.fn();
		const uniqueId = jest.fn();
		const callback = jest.fn();
		const codeTypeResponse = {
			codeId,
			username: '',
			isInvitation: false,
			isUsername: false,
		};
		const codeType = new CodeType(isInvitation, isUsername, uniqueId);
		await codeType.get(codeId, callback);
		expect(isInvitation).toHaveBeenCalledWith(codeId);
		expect(uniqueId).not.toBeCalled();
		expect(isUsername).not.toBeCalled();
		expect(callback).toBeCalledWith(error, codeTypeResponse);
	});
});
