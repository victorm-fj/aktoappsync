// import { saveUser, deleteUser } from '../../../src/helpers/dynamodb';
// import { handler } from '../../../src/functions/getCodeType';

// describe('getCodeType lambda', () => {
// 	const userId = 'us-east-1:3fbbd145-5f54-4e45-8e36-9bb70ea1defx';
// 	const username = 'ERG67JK';
// 	const invitationId = 'JKT4D3A';
// 	beforeEach(async () => {
// 		process.env.USERS_TABLE = 'aktodev-mobilehub-1748638208-Users';
// 		await saveUser(userId, username, invitationId);
// 	});
// 	afterEach(async () => {
// 		await deleteUser(userId);
// 	});
// 	test('getCodeType with isValidCode prop set to true', async () => {
// 		const event = {
// 			arguments: { accessId: invitationId },
// 		};
// 		const callback = jest.fn();
// 		await handler(event, {}, callback);
// 		expect(response.accessId).not.toBe(invitationId);
// 		expect(response.isValidCode).toBe(true);
// 		expect(response.isActiveUser).toBe(false);
// 		expect(callback).toHaveBeenCalledWith(null, response);
// 	});
// });
