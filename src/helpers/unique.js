import generate from 'nanoid/generate';

export const randomId = () =>
	generate('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', 7);

export const uniqueId = async fn => {
	const id = randomId();
	const data = await fn(id);
	if (data.Count > 0) {
		// There's an existing record with the same id in DB
		console.log('not an unique id');
		return uniqueId(fn);
	} else {
		// Does not exists in DB
		console.log('unique id');
		return id;
	}
};
