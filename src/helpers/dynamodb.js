const AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });

export const call = (action, params) => {
	const dynamoDB = new AWS.DynamoDB.DocumentClient();
	return dynamoDB[action](params).promise();
};

export const isUser = userId => {
	const params = {
		TableName: process.env.USERS_TABLE,
		Key: { userId },
	};
	return call('get', params);
};

export const isUsername = username => {
	const params = {
		TableName: process.env.USERS_TABLE,
		IndexName: 'username',
		KeyConditionExpression: 'username = :username',
		ExpressionAttributeValues: {
			':username': username,
		},
	};
	return call('query', params);
};

export const isInvitation = invitationId => {
	const params = {
		TableName: process.env.USERS_TABLE,
		IndexName: 'invitationId',
		KeyConditionExpression: 'invitationId = :invitationId',
		ExpressionAttributeValues: {
			':invitationId': invitationId,
		},
	};
	return call('query', params);
};

export const saveUser = item => {
	const params = {
		TableName: process.env.USERS_TABLE,
		Item: item,
	};
	return call('put', params);
};

export const deleteUser = userId => {
	const params = {
		TableName: process.env.USERS_TABLE,
		Key: { userId },
	};
	return call('delete', params);
};
