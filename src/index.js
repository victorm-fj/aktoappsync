import getCodeType from './functions/getCodeType';
import newUser from './functions/newUser';
import inviteRecruits from './functions/inviteRecruits';

export const handler = (event, context, callback) => {
	console.log('Received event {}', JSON.stringify(event, undefined, 2));
	switch (event.field) {
	case 'getCodeType':
		getCodeType(event, callback);
		break;
	case 'newUser':
		newUser(event, callback);
		break;
	case 'inviteRecruits':
		inviteRecruits(event, callback);
		break;
	default:
		callback(`Unknown field, unable to resolve ${event.field}`, null);
		break;
	}
};
