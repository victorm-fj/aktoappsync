import { isInvitation, saveUser } from '../../helpers/dynamodb';
import { uniqueId } from '../../helpers/unique';
import NewUsers from './NewUsers';

const newUsers = new NewUsers(uniqueId, isInvitation, saveUser);

export default (event, callback) => {
	console.log('newUser invoked');
	const {
		arguments: { username, invitation },
		identity: { cognitoIdentityId },
	} = event;
	newUsers.save(cognitoIdentityId, username, invitation, callback);
};
