class NewUsers {
	constructor(uniqueId, isInvitation, saveUser) {
		this.uniqueId = uniqueId;
		this.isInvitation = isInvitation;
		this.saveUser = saveUser;
	}
	async save(userId, username, invitation, callback) {
		try {
			// invitation is the recruiter invitation
			const data = await this.isInvitation(invitation);
			console.log('recruiter data -', data);
			const recruiter = data.Items[0];
			const recruits = recruiter.recruits;
			const updatedRecruiter = {
				...recruiter,
				recruits: [...recruits, username],
			};
			console.log('updated recruiter -', updatedRecruiter);
			await this.saveUser(updatedRecruiter);
			// get a unique invitationId for the new user
			const invitationId = await this.uniqueId(this.isInvitation);
			const newUser = {
				userId,
				username,
				invitationId,
				recruiter: recruiter.username,
				recruits: [],
			};
			await this.saveUser(newUser);
			console.log('successfully saved new user', newUser);
			callback(null, newUser);
		} catch (error) {
			console.log('save new user error', error);
			callback(error, null);
		}
	}
}

export default NewUsers;
