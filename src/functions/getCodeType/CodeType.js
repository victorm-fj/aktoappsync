class CodeType {
	constructor(isInvitation, isUsername, uniqueId) {
		this.isInvitation = isInvitation;
		this.isUsername = isUsername;
		this.uniqueId = uniqueId;
	}
	async get(codeId, callback) {
		const codeType = {
			codeId,
			username: '',
			isInvitation: false,
			isUsername: false,
		};
		try {
			const data = await this.isInvitation(codeId);
			console.log('isInvitationId data', data);
			if (data.Count > 0) {
				// The codeId corresponds to an existing invitationId
				const username = await this.uniqueId(this.isUsername);
				codeType.username = username;
				codeType.isInvitation = true;
				console.log('getCodeType response -', codeType);
				callback(null, codeType);
			} else {
				// codeId is not a valid invitationId, check if it is an existing User's username
				const data = await this.isUsername(codeId);
				if (data.Count > 0) {
					// If accessId is a valid User's username
					codeType.isUsername = true;
					console.log('getCodeType response -', codeType);
					callback(null, codeType);
				} else {
					console.log('getCodeType response -', codeType);
					callback(null, codeType);
				}
			}
		} catch (error) {
			console.log('getCodeType error -', error);
			callback(error, codeType);
		}
	}
}

export default CodeType;

// const isInvitationIdResponseExample = {
// 	Items: [{ invitationId: 'POWGLUF', userId: '6ADLFPF' }],
// 	Count: 1,
// 	ScannedCount: 1,
// };
// const isNotValidInvitationIdResponseExample = {
// 	Items: [],
// 	Count: 0,
// 	ScannedCount: 0,
// };
