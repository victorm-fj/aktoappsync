import { isInvitation, isUsername } from '../../helpers/dynamodb';
import { uniqueId } from '../../helpers/unique';
import CodeType from './CodeType';

const codeType = new CodeType(isInvitation, isUsername, uniqueId);

export default (event, callback) => {
	console.log('getCodeType invoked');
	const { codeId } = event.arguments;
	codeType.get(codeId, callback);
};

// const eventExampleIAMAuthType = {
// 	field: 'getCodeType',
// 	arguments: { codeId: '6FGBNRG' },
// 	identity: {
// 		accountId: '441343615666',
// 		cognitoIdentityPoolId: 'us-east-1:7a66ee4e-1c66-4cee-b0ac-101d2cab3633',
// 		cognitoIdentityId: 'us-east-1:3fbbd145-5f54-4e45-8e36-9bb70ea1def2',
// 		sourceIp: '10.0.27.226',
// 		username: 'AROAICTMNW76M4CHGOCFW:CognitoIdentityCredentials',
// 		userArn:
// 			'arn:aws:sts::441343615666:assumed-role/aktodev_unauth_MOBILEHUB_1748638208/CognitoIdentityCredentials',
// 	},
// };
