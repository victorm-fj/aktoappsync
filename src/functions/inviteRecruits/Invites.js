class Invites {
	constructor(isUser, sendMessage) {
		this.isUser = isUser;
		this.sendMessage = sendMessage;
	}
	async send(userId, recruits, downloadURL, callback) {
		try {
			// Get the invitationId that belongs to the user who is sending the invitations
			const data = await this.isUser(userId);
			const results = await Promise.all(
				recruits.map(recruit =>
					this.sendMessage(
						recruit.phoneNumber,
						data.Item.invitationId,
						downloadURL
					)
				)
			);
			console.log("successfully sent SMS's to recruits", results);
			callback(null, recruits);
		} catch (error) {
			console.log("error sending SMS's to recruits", error);
			callback(error, null);
		}
	}
}

export default Invites;
