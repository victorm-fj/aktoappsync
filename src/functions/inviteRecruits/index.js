import { sendMessage } from '../../helpers/sns';
import { isUser } from '../../helpers/dynamodb';
import Invites from './Invites';

// App's webpage download link
const downloadURL = 'akto.io';
const invites = new Invites(isUser, sendMessage);

export default (event, callback) => {
	console.log('inviteRecruits invoked');
	// Data from the 'invite' mutation event
	const {
		arguments: { recruits },
		identity: { cognitoIdentityId },
	} = event;
	invites.send(cognitoIdentityId, recruits, downloadURL, callback);
};

// const eventExampleIAMAuthType = {
// 	field: 'inviteRecruits',
// 	arguments: {
// 		userId: 'BPNEWSV',
// 		recruits: [
// 			{
// 				recordId: '88AA459A-36B0-4BF3-8F7C-AE3181C8C243:ABPerson',
// 				phoneNumber: '+527351606138',
// 			},
// 		],
// 	},
// 	identity: {
// 		accountId: '441343615666',
// 		cognitoIdentityPoolId: 'us-east-1:7a66ee4e-1c66-4cee-b0ac-101d2cab3633',
// 		cognitoIdentityId: 'us-east-1:016ef7b7-bd37-421d-ad6f-ddc1eb463413',
// 		sourceIp: '10.0.159.203',
// 		username: 'AROAJUKJ6ORM5ZMI2NGW6:CognitoIdentityCredentials',
// 		userArn:
// 			'arn:aws:sts::441343615666:assumed-role/aktodev_auth_MOBILEHUB_1748638208/CognitoIdentityCredentials',
// 	},
// };
